﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace BubbleSortConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            const int count = 100;
            
            var random = new Random();

            var nums = Enumerable.Range(1, count).Select(s => random.Next(10, count)).ToArray();

            var stopwatch = new Stopwatch();

            using var streamWriter =new StreamWriter("D:\\test.txt");

            stopwatch.Restart();

            LinqSort(nums);

            stopwatch.Stop();

            streamWriter.Write($"linq sort duration in {count} times is : {stopwatch.ElapsedMilliseconds} Msec\n");

            stopwatch.Restart();

            BublleSort(nums);

            stopwatch.Stop();

            streamWriter.Write($"bubble sort duration in {count} times is : {stopwatch.ElapsedMilliseconds} Msec");
        }

        private static void LinqSort(int[] nums)
        {
            foreach (var num in nums.OrderBy(o => o).ToList())
            {
                Console.WriteLine(num);
            }
        }

        private static void BublleSort(int[] nums)
        {
            var temp = 0;

            for (var write = 0; write < nums.Length; write++)
            {
                for (var sort = 0; sort < nums.Length - 1; sort++)
                {
                    if (nums[sort] > nums[sort + 1])
                    {
                        temp = nums[sort + 1];
                        nums[sort + 1] = nums[sort];
                        nums[sort] = temp;
                    }
                }
            }
            foreach (var num in nums)
            {
                Console.WriteLine(num);
            }
        }
    }
}
